from django import template
from django.contrib.humanize.templatetags.humanize import intcomma
from wishlist.models import Wishlist

register = template.Library()

@register.filter(name="index", is_safe=True)
def index(value, arg):
    value = True
    wishlist = Wishlist.objects.all()
    for i in wishlist :
        if i.barang_id == arg :
            value = False
    return value