from django.urls import path
from . import views

app_name='listfavorit'

urlpatterns = [
    path('listfavorit/', views.listfavorit, name='listfavorit'),
    path('listfavorit/<int:idbarang>', views.detailListFavorit, name='detailListFavorit'),
    path('listfavorit/add/<int:idbarang>/', views.addListFavorit, name='addListFavorit'),
    path('listfavorit/delete/<int:idbarang>/', views.deleteListFavorit, name='deleteListFavorit'),
    path('checkout/', views.checkout, name='checkout'),
]