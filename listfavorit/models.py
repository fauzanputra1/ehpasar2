from django.db import models
from detailBarang.models import Barang
from ehPasar.settings import AUTH_USER_MODEL as User

# Create your models here.

class Listfavorit(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    idbarang = models.ForeignKey(Barang, on_delete=models.CASCADE)