from django.apps import AppConfig


class ListfavoritConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'listfavorit'
