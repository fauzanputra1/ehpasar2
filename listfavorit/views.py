from django.http import response
from django.shortcuts import render, redirect
from .models import Listfavorit
from .forms import ListfavoritForm
from detailBarang.models import Barang

# Create your views here.

def listfavorit(request):
	if 'nama' not in request.session:
		return redirect('main:home')
	user_type = request.session['role']
	
	if user_type == "pembeli" :
		listfavorit = Listfavorit.objects.all()
		response ={
			'listfavorit' : listfavorit
		}
		return render(request, 'listfavorit.html',response)

def addListFavorit(request, idbarang):
	if 'nama' not in request.session:
		return redirect('main:home')
	user_type = request.session['role']
	
	if user_type == "pembeli" :
		if (request.method == 'POST'):
			form = ListfavoritForm(request.POST)
			form.instance.user = request.user
			if (form.is_valid()):
				barang = Barang.objects.filter(id=idbarang)[0]
				listfavorit = form.save()
				listfavorit.barang = barang
				listfavorit.save()
				return redirect('listBarang:listBarang')


def detailListFavorit(request, idbarang):
	if 'nama' not in request.session:
		return redirect('main:home')
	user_type = request.session['role']
	
	if user_type == "pembeli" :
		barang = Listfavorit.objects.get(id=idbarang)
		response={
			'barang' : barang,
		}
		return render(request, 'detaillistfavorit.html',response)

def deleteListFavorit(request, idbarang):
	if 'nama' not in request.session:
		return redirect('main:home')
	user_type = request.session['role']
	
	if user_type == "pembeli" :
		if (request.method == 'POST'):
			barang = Listfavorit.objects.filter(id=idbarang).delete()
		return render(request, 'listfavorit.html')

def checkout(request):
	if 'nama' not in request.session:
		return redirect('main:home')
	user_type = request.session['role']

	if user_type == "pembeli" :
		listfavorit = Listfavorit.objects.all()
		price = 0
		for listf in listfavorit:
			price+= listf.idbarang.hargaBarang
		response ={
			'price' : price,
			'total' : price + 30000,
			'listfavorit' :listfavorit,
		}
		return render(request, 'checkout.html', response)