from django import template
from django.contrib.humanize.templatetags.humanize import intcomma

register = template.Library()

@register.filter
def priceFilter(price):
    if (price):
        return "Rp. %s" % (intcomma(int(price)))
    return ''

@register.filter

def makeRange(num, arg):
    return range(abs(int(arg)-int(num)))

