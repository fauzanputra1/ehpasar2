from django.shortcuts import render, redirect
from .models import Barang
from django.http import JsonResponse

def detailBarang(request, barang_id):
    if 'nama' not in request.session:
        return redirect('main:home')
    user_type = request.session['role']
    
    if user_type == "pembeli" :
        response = {}
        post = Barang.objects.filter(id=barang_id)[0]
        response['post'] = post
        return render(request, 'detailBarang/postdetails.html', response)
