from django.db import models
from ehPasar.settings import AUTH_USER_MODEL as User

# Create your models here.
class Barang(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    namaBarang = models.CharField(max_length=100)
    jumlahStok = models.PositiveIntegerField()
    hargaBarang = models.PositiveIntegerField()
    deskripsiBarang = models.CharField(max_length=300)
    def __str__(self):
        return self.namaBarang
