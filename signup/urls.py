from django.urls import path
from . import views

app_name='signup'

urlpatterns = [
    path('signup', views.daftar, name='daftar'),
    path('coba', views.coba, name='coba'),
    path('masuk', views.signup, name='signup'),
    path('sign_in', views.signin, name='sign_in'),
    path('sign_out', views.signOut, name='sign_out'),
]