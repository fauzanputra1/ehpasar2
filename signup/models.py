from django.db import models
from django.utils import timezone
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin, User

# Create your models here.
class UserManager(BaseUserManager):

    def _create_user(self, nama, password, is_superuser, is_staff,
                    email, nomorTelf, role, **extra_fields):
        if not nama:
            raise ValueError('User must have a valid username')
        now = timezone.now()
        email = self.normalize_email(email)
        user = self.model(
            nama=nama,
            email = email,
            nomorTelf = nomorTelf,
            role = role,
            last_login = now,
            date_joined = now,
            is_superuser = is_superuser,
            is_staff = is_staff,
            **extra_fields
        )
        user.set_password(password)
        user.save(using=self._db)
        return user
    
    def create_user(self, nama, password, **extra_fields):
        return self._create_user(nama, password, False, False, **extra_fields)

    def create_superuser(self, username, password, **extra_fields):
        user = self._create_user(username, password, True, True, **extra_fields)
        user.save(using=self._db)
        return user


class User(AbstractBaseUser, PermissionsMixin) :
    nama = models.CharField(max_length=50, unique=True)
    email = models.EmailField(max_length=255)
    nomorTelf = models.CharField(max_length=13)
    role = models.CharField(max_length=7)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)
    date_joined = models.DateTimeField(auto_now_add=True)
    last_login = models.DateTimeField(null=True, blank=True)


    USERNAME_FIELD = 'nama'
    EMAIL_FIELD = 'email'
    REQUIRED_FIELDS = ['email', 'nomorTelf', 'role']

    objects = UserManager()

    def get_absolute_url(self):
        return "/users/%i" % (self.pk)
