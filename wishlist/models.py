from django.db import models
from django.db.models.fields import IntegerField
from detailBarang.models import Barang
from ehPasar.settings import AUTH_USER_MODEL as User
# Create your models here.

class Wishlist(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    barang_id = models.ForeignKey(Barang, on_delete=models.CASCADE)