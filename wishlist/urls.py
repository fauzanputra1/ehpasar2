from django.urls import path
from . import views

app_name='wishlist'

urlpatterns = [
    path('wishlist/', views.getWishlist, name='wishlist'),
    path('add_wishlist/<int:barang_id>/', views.insertBarangWishlist, name='add_wishlist'),
    path('wishlist/detail_wishlist/<int:barang_id>/', views.detail_wishlist, name='detail_wishlist'),
    path('detele_wishlist/<int:barang_id>/', views.removeBarangWishlist, name='delete_wishlist'),
]