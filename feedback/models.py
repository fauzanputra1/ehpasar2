from django.db import models
from django.conf import settings
from detailBarang.models import Barang
from ehPasar.settings import AUTH_USER_MODEL as User

# Create your models here.
class Feedback(models.Model):
    idbarang = models.ForeignKey(Barang, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    date = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    content = models.TextField(max_length = 500)