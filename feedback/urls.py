from django.urls import path
from . import views

app_name = "feedback"

urlpatterns = [
    path('feedback/', views.feedback, name='feedback'),
    path('feedback/savefeedback', views.savefeedback, name='savefeedback'),
    path('feedback/delete/<int:idBarang>', views.deletefeedback, name='deletefeedback'),
    path('feedback/edit/<int:idBarang>', views.editfeedback, name='editfeedback')
]