from django.shortcuts import render, redirect, get_object_or_404, reverse
from .models import Feedback
from .forms import FeedbackForm
from detailBarang.models import Barang
from django.http import response
from django.http import HttpResponseRedirect

# Create your views here.
def feedback(request):
    if 'nama' not in request.session:
        return redirect('main:home')
    user_type = request.session['role']

    if user_type == 'pembeli':
        feedback = Feedback.objects.all()
        response ={
            'feedback' : feedback
        }
        return render(request, 'feedback.html', response)

def savefeedback (request):
    barang = Barang.objects.all()
    if(request.method == 'POST'):
        form = FeedbackForm(request.POST)
        if form.is_valid():
            feedback = form.save(commit=False)
            feedback.save()
            return HttpResponseRedirect('/feedback/')
    else:
        context ={
        'feedback_form' : FeedbackForm,
        'barang' : barang,
    }
    return render(request, "savefeedback.html", context)

def deletefeedback(request, idBarang):
    if request.method == 'POST':
        barang = Feedback.objects.filter(id=idBarang).delete()
    return render(request, 'feedback.html')

def editfeedback(request, idBarang=None):
    if idBarang:
        feedback = get_object_or_404(Feedback, pk=idBarang)
    form = FeedbackForm(request.POST, instance=feedback)
    barang = Barang.objects.all()
    if request.method == "POST":
        if form.is_valid():
            feedback = form.save(commit=False)
            feedback.save()
            return HttpResponseRedirect('/feedback/')
    else:
        form = FeedbackForm(instance=feedback)
        context ={
        'feedback_form' : FeedbackForm,
        'barang' : barang
    }
    return render(request, "savefeedback.html", context)